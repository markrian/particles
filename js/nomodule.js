// eslint-disable-next-line prefer-arrow-callback
document.addEventListener('DOMContentLoaded', function () {
    const element = document.querySelector('#browser-not-supported').cloneNode(true);
    element.className = 'error-header';

    document.body.innerHTML = '';
    document.body.append(element);
});
