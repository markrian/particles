import state from './state.js';
import { canvas } from './canvas.js';

const windowEvents = {
    resize() {
        state.width = window.innerWidth;
        state.height = window.innerHeight;

        canvas.width = state.width;
        canvas.height = state.height;
    },
};

export default windowEvents;
