const transitionend = 'transitionend';

export async function onTransitionEnd(element) {
    if (element.style.transition === undefined) {
        return;
    }

    return new Promise(resolve => {
        const timeout = setTimeout(() => {
            console.warn('timeout waiting for transitionend', element);
            resolve();
        }, 1000);

        const listener = function () {
            element.removeEventListener(transitionend, listener);
            resolve();
            clearTimeout(timeout);
        };

        element.addEventListener(transitionend, listener);
    });
}

// Ensure CSS transitions triggered by JS toggling classes always work
export function doubleRAF(fn) {
    return requestAnimationFrame(() => requestAnimationFrame(fn));
}
